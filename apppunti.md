**GIT CHEATSHEET**
-------
My personal survival guide to git :3
----------
**Git basics** 
--------
Use git help [command] if you're stuck 

*master* --> default devel branch   
*origin* --> default upstream branch   
*HEAD* --> current branch  
*HEAD^* --> parent of head  
*foo..bar* --> from branch foo to branch bar
-------
**Create**
-------
create from existing files  
*git init* --> will create a new local git repository   
*git add .*  
create from existing repository   
*git clone* --> it's used to copy a repository   
*git clone ~/old ~/new*  
*git clone git://...*  
*git clone ssh://...*  

------
**View**
-------
*git status* --> list new or modified files, not yet committed  
*git diff* --> lists down changes and conflicts   
*git log* --> show all commits in the current branch's history  
*git blame*  
*git show id*  
*git show id:file*  
*git tag -l* --> shows list  
-----
**Revert**
-------
*git reset [file]* --> unstages file, keeping the file changes   
*git reset --hard* --> reset to last command   
*git revert*  
----
**Publish**
-------
*git commit [-a]* --> will create a snapshot of the changes and save it to the git directory  
*git push* --> push local changes to the original commit  
-----
**Make a change**
-------
*git add [file]* --> stages the file, ready for commit   
*git commit -m "[descriptive message]"* --> commit all stages files to versioned   history   
*git pull* (fetch and merge)   
-----
**Git branching & merging**
-------
*git branch* --> will list your branches   
*git merge* --> will merge the specified branch's history into the current branch  
*git checkout -b*  
-----
**Synchronize**
-------
*git remote add <name> <url>* --> create a new connection to a remote repo  
*git fetch* --> get all the changes from the the origin (no merge)   
*git pull* --> get all the latest changes from the origin and merge   
*git push* it's used to upload your local repository changes to the origin remote repo  
-------
**Useful tools**
------
*git archive* --> create release tarball  
*git bisects* --> binary search for defects  
*git cherry-pick* --> take single commit from elsewhere   
*git fsck* --> check tree  
*git gc* --> compress metadata (performance)  
*git rebase* --> forward-port local changes to remote branch  
*git stash* --> temporaly set aside changes  
-----
**Conflicts**
-------
use add to mark files as resolved  
*git diff [--base]*   
*git diff --ours*  
*git diff --theirs*  
*git log --merge*  
-------
**Tracking files**
-------
*git add*   
*git mv old new*   
*git rm files*  
*git rm --cached files*  
--------

*References:*
- last update 2023: https://education.github.com/git-cheat-sheet-education.pdf  
- last update 2022: https://www.freecodecamp.org/news/git-cheat-sheet/
